import logging

import google.generativeai as genai

from .entities import GenerativeModel
from .exceptions import ConfigureError

logger: logging.Logger = logging.getLogger('ai')

text_model: GenerativeModel | None = None
vision_model: GenerativeModel | None = None


def get_text_model() -> GenerativeModel:
    global text_model

    if not text_model:
        raise ValueError('text_model not initialized')
    return text_model


def get_vision_model() -> GenerativeModel:
    global vision_model

    if not vision_model:
        raise ValueError('vision_model not initialized')
    return vision_model


def configure(
    api_key: str,
    text_model_name: str,
    vision_model_name: str,
) -> None:
    global text_model, vision_model

    genai.configure(api_key=api_key)
    supported_models: list[str] = [
        m.name for m in genai.list_models()
        if 'generateContent' in m.supported_generation_methods
    ]
    logger.info('Supported models: %s', supported_models)
    if (
        'models/gemini-pro' not in supported_models
        or 'models/gemini-pro-vision' not in supported_models
    ):
        raise ConfigureError('No required models')

    text_model = GenerativeModel(text_model_name)
    vision_model = GenerativeModel(vision_model_name)
