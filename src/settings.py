from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    debug: bool = False

    google_api_key: str
    text_model: str = 'gemini-pro'
    vision_model: str = 'gemini-pro-vision'

    telegram_bot_token: str
    telegram_users_whitelist: set[int]


settings = Settings()
