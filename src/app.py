import asyncio
import logging
import sys

from typing import Coroutine

from ai import (
    ConfigureError as AIConfigureError,
    configure as ai_configure,
)
from settings import settings
from telegram import (
    ConfigureError as TelegramConfigureError,
    configure as telegram_configure,
)

logger: logging.Logger = logging.getLogger('app')
logging.basicConfig(
    level=logging.DEBUG if settings.debug else logging.INFO,
    stream=sys.stdout,
)


if __name__ == '__main__':
    logger.info('Configuring...')

    try:
        ai_configure(
            api_key=settings.google_api_key,
            text_model_name=settings.text_model,
            vision_model_name=settings.vision_model,
        )
    except AIConfigureError:
        logger.exception('Failed to configure AI')
        exit(1)

    try:
        run_telegram: Coroutine[None, None, None] = telegram_configure(
            bot_token=settings.telegram_bot_token,
            users_whitelist=settings.telegram_users_whitelist,
        )
    except TelegramConfigureError:
        logger.exception('Failed to configure Telegram')
        exit(1)

    logger.info('Configured')
    logger.info('Starting...')
    asyncio.run(run_telegram)
