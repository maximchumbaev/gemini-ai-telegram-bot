import logging

from typing import Coroutine

from aiogram import (
    Bot,
    Dispatcher,
)
from aiogram.enums import ParseMode
from aiogram.filters import CommandStart
from aiogram.types import Message
from aiogram.utils.token import TokenValidationError

from services import (
    start_text_chat,
    stream_text_request,
)
from .exceptions import ConfigureError

logger: logging.Logger = logging.getLogger('telegram')

whitelist: set[int] = set()

dispatcher: Dispatcher = Dispatcher()
bot: Bot | None = None


def configure(
    bot_token: str,
    users_whitelist: set[int],
) -> Coroutine[None, None, None]:
    global bot, whitelist

    try:
        bot = Bot(bot_token, parse_mode=ParseMode.MARKDOWN_V2)
    except TokenValidationError:
        raise ConfigureError('Invalid bot token')

    whitelist = users_whitelist
    return dispatcher.start_polling(bot)


@dispatcher.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    if message.from_user.id not in whitelist:
        await message.answer(
            'Sorry, you have no access to this bot',
            parse_mode=ParseMode.HTML,
        )
        logger.debug(
            'User %s (%s) tried to use bot',
            message.from_user.username,
            message.from_user.id,
        )
        return

    start_text_chat(message.from_user.id)
    await message.answer(
        f'Hello, {message.from_user.username}!',
        parse_mode=ParseMode.HTML,
    )


@dispatcher.message()
async def message_handler(message: Message) -> None:
    if message.from_user.id not in whitelist:
        await message.answer(
            'Sorry, you have no access to this bot',
            parse_mode=ParseMode.HTML,
        )
        logger.debug(
            'User %s (%s) tried to use bot',
            message.from_user.username,
            message.from_user.id,
        )
        return

    current_message: str = ''
    current_message_id: int | None = None
    for chunk in stream_text_request(message.from_user.id, message.text):
        if len(current_message + chunk) > 4096:
            msg: Message = await message.answer(
                chunk,
                parse_mode=ParseMode.HTML,
            )
            current_message_id = msg.message_id
            current_message = chunk
        else:
            current_message += chunk
            if current_message_id:
                await bot.edit_message_text(
                    current_message,
                    chat_id=message.chat.id,
                    message_id=current_message_id,
                    parse_mode=ParseMode.HTML,
                )
            else:
                msg: Message = await message.answer(
                    current_message,
                    parse_mode=ParseMode.HTML,

                )
                current_message_id = msg.message_id
