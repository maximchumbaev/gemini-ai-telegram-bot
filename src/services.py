from typing import Generator

from ai import get_text_model
from ai.entities import (
    ChatSession,
    GenerativeModel,
    GenerateContentResponse,
)

CHATS: dict[int, ChatSession] = {}


def stream_text_request(user_id: int, text: str) -> Generator[str, None, None]:
    chat: ChatSession | None = CHATS.get(user_id)
    if not chat:
        chat: ChatSession = start_text_chat(user_id)

    response: GenerateContentResponse = chat.send_message(
        text,
        stream=True,
    )

    for chunk in response:
        yield chunk.text


def start_text_chat(user_id: int) -> ChatSession:
    model: GenerativeModel = get_text_model()

    chat: ChatSession = model.start_chat()
    CHATS.update({user_id: chat})

    return chat
