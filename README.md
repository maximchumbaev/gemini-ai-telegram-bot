Google Gemini AI chatbot for Telegram
-

### Before run:
#### Generate Google API key
go to https://makersuite.google.com/app/apikey and generate the API key
#### Create a Telegram bot
- find a bot `@BotFather` and create a new bot following the BotFather's instructions
#### Fill the telegram users whitelist
- run the `app.py` and wait until it initialized
- find your bot and write a message to it
- look at the logs and find message: `User <user_name> (<user_id>) tried to use bot`
- copy user_id from log and paste it to `TELEGRAM_USERS_WHITELIST`
- [optional] set `DEBUG` to `False`
- restart the app

### Usage
To start or restart a chat session with Gemini, send `/start`

### Docker
```shell
docker run -d --name gemini-bot -e GOOGLE_API_KEY=<api_key> -e TELEGRAM_BOT_TOKEN=<token> -e TELEGRAM_USERS_WHITELIST='[<tokens>]' -e DEBUG=True madmadmax/gemini-tg-bot:latest
```
