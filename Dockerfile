FROM python:3.11-slim

WORKDIR /app

RUN pip install pipenv

COPY Pipfile Pipfile.lock /app/
RUN pipenv install --deploy --ignore-pipfile

COPY src/ /app/src/

CMD ["pipenv", "run", "python", "src/app.py"]
